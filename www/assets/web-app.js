'use strict';



;define("web-app/adapters/-json-api", ["exports", "@ember-data/adapter/json-api"], function (_exports, _jsonApi) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _jsonApi.default;
    }
  });
});
;define("web-app/app", ["exports", "ember-resolver", "ember-load-initializers", "web-app/config/environment"], function (_exports, _emberResolver, _emberLoadInitializers, _environment) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  class App extends Ember.Application {
    constructor(...args) {
      super(...args);

      _defineProperty(this, "modulePrefix", _environment.default.modulePrefix);

      _defineProperty(this, "podModulePrefix", _environment.default.podModulePrefix);

      _defineProperty(this, "Resolver", _emberResolver.default);
    }

  }

  _exports.default = App;
  (0, _emberLoadInitializers.default)(App, _environment.default.modulePrefix);
});
;define("web-app/component-managers/glimmer", ["exports", "@glimmer/component/-private/ember-component-manager"], function (_exports, _emberComponentManager) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _emberComponentManager.default;
    }
  });
});
;define("web-app/components/c-4", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  const __COLOCATED_TEMPLATE__ = Ember.HTMLBars.template(
  /*
    <h2 class="info text-center">Select a column to drop your counter</h2>
  <h4 class="info text-center">Get 4 counters in a row to win!</h4>
  {{!-- HTML canvas to draw game on --}}
  <canvas id="stage" width="325" height="350"></canvas>
  {{#if playing}}
  {{#if winner}}
  <div class="result">
      {{winner}} player won!
  </div>
  {{/if}}
  {{#if draw}}
  <div class="result">
      It's a draw !!
  </div>
  {{/if}}
  <button {{action 'start' }}>Restart</button>
  {{else}}
  <button {{action 'start' }}>Play Connect 4</button>
  {{/if}}
  */
  {
    "id": "ezDG2acP",
    "block": "{\"symbols\":[],\"statements\":[[10,\"h2\"],[14,0,\"info text-center\"],[12],[2,\"Select a column to drop your counter\"],[13],[2,\"\\n\"],[10,\"h4\"],[14,0,\"info text-center\"],[12],[2,\"Get 4 counters in a row to win!\"],[13],[2,\"\\n\"],[10,\"canvas\"],[14,1,\"stage\"],[14,\"width\",\"325\"],[14,\"height\",\"350\"],[12],[13],[2,\"\\n\"],[6,[37,2],[[35,4]],null,[[\"default\",\"else\"],[{\"statements\":[[6,[37,2],[[35,1]],null,[[\"default\"],[{\"statements\":[[10,\"div\"],[14,0,\"result\"],[12],[2,\"\\n    \"],[1,[34,1]],[2,\" player won!\\n\"],[13],[2,\"\\n\"]],\"parameters\":[]}]]],[6,[37,2],[[35,3]],null,[[\"default\"],[{\"statements\":[[10,\"div\"],[14,0,\"result\"],[12],[2,\"\\n    It's a draw !!\\n\"],[13],[2,\"\\n\"]],\"parameters\":[]}]]],[11,\"button\"],[4,[38,0],[[32,0],\"start\"],null],[12],[2,\"Restart\"],[13],[2,\"\\n\"]],\"parameters\":[]},{\"statements\":[[11,\"button\"],[4,[38,0],[[32,0],\"start\"],null],[12],[2,\"Play Connect 4\"],[13],[2,\"\\n\"]],\"parameters\":[]}]]]],\"hasEval\":false,\"upvars\":[\"action\",\"winner\",\"if\",\"draw\",\"playing\"]}",
    "meta": {
      "moduleName": "web-app/components/c-4.hbs"
    }
  }); // ****** Wade Phillips (23944129) - CIS3158 CW02 - Connect 4 ******


  /**
   * Connect 4 component  - contains code for: 
   * Creating and drawing the board and counters.
   * Implementing a human and computer player.
   * Determining a winner via supplied pattern recognition.
   */

  /**
   * deepClone function clones the state matrix; 
   * uses a for loop to makes copies of the game board for the minimax
   * algorithm to create the game tree of possible future moves for
   * the current board state.
   * Out puts a list (new_state) of matrix.
   * @param {*} state 
   */
  function deepClone(state) {
    // Initiate new variable to hold new state matrixs
    var new_state = []; // Loops through x axis up to its length

    for (var x = 0; x < state.length; x++) {
      // adds board matric to new variable.
      new_state.push(state[x].slice(0));
    }

    return new_state;
  }
  /**
   * Originally inside the component to check for a winner between two human players.
   * Function provides all possible winning counter positions for the board using x,y co-ordinates.
   * Patterns are held in nested arrays within the patterns array.
   * 
   * @param {*} state 
   */


  function check_game_winner(state) {
    var patterns = [// x,y co-ordinates used to provide winning positions for counters on board
    // 1st row patterns (top) - horizontal patterns
    // 4 for each row as 7 columns wide
    [[0, 0], [1, 0], [2, 0], [3, 0]], [[1, 0], [2, 0], [3, 0], [4, 0]], [[2, 0], [3, 0], [4, 0], [5, 0]], [[3, 0], [4, 0], [5, 0], [6, 0]], // 2nd row patterns
    [[0, 1], [1, 1], [2, 1], [3, 1]], [[1, 1], [2, 1], [3, 1], [4, 1]], [[2, 1], [3, 1], [4, 1], [5, 1]], [[3, 1], [4, 1], [5, 1], [6, 1]], // 3rd row patterns
    [[0, 2], [1, 2], [2, 2], [3, 2]], [[1, 2], [2, 2], [3, 2], [4, 2]], [[2, 2], [3, 2], [4, 2], [5, 2]], [[3, 2], [4, 2], [5, 2], [6, 2]], // 4th row patterns
    [[0, 3], [1, 3], [2, 3], [3, 3]], [[1, 3], [2, 3], [3, 3], [4, 3]], [[2, 3], [3, 3], [4, 3], [5, 3]], [[3, 3], [4, 3], [5, 3], [6, 3]], // 5th row patterns
    [[0, 4], [1, 4], [2, 4], [3, 4]], [[1, 4], [2, 4], [3, 4], [4, 4]], [[2, 4], [3, 4], [4, 4], [5, 4]], [[3, 4], [4, 4], [5, 4], [6, 4]], // 6th row patterns (bottom)
    [[0, 5], [1, 5], [2, 5], [3, 5]], [[1, 5], [2, 5], [3, 5], [4, 5]], [[2, 5], [3, 5], [4, 5], [5, 5]], [[3, 5], [4, 5], [5, 5], [6, 5]], // 1st column (left) - vertical patterns
    // 3 for each  column as 6 rows high
    [[0, 0], [0, 1], [0, 2], [0, 3]], [[0, 1], [0, 2], [0, 3], [0, 4]], [[0, 2], [0, 3], [0, 4], [0, 5]], // 2nd column
    [[1, 0], [1, 1], [1, 2], [1, 3]], [[1, 1], [1, 2], [1, 3], [1, 4]], [[1, 2], [1, 3], [1, 4], [1, 5]], // 3rd column
    [[2, 0], [2, 1], [2, 2], [2, 3]], [[2, 1], [2, 2], [2, 3], [2, 4]], [[2, 2], [2, 3], [2, 4], [2, 5]], // 4th column
    [[3, 0], [3, 1], [3, 2], [3, 3]], [[3, 1], [3, 2], [3, 3], [3, 4]], [[3, 2], [3, 3], [3, 4], [3, 5]], // 5th column
    [[4, 0], [4, 1], [4, 2], [4, 3]], [[4, 1], [4, 2], [4, 3], [4, 4]], [[4, 2], [4, 3], [4, 4], [4, 5]], // 6th column
    [[5, 0], [5, 1], [5, 2], [5, 3]], [[5, 1], [5, 2], [5, 3], [5, 4]], [[5, 2], [5, 3], [5, 4], [5, 5]], // 7th column (right)
    [[6, 0], [6, 1], [6, 2], [6, 3]], [[6, 1], [6, 2], [6, 3], [6, 4]], [[6, 2], [6, 3], [6, 4], [6, 5]], // Diagonal patterns
    // 1st column moving down + right
    [[0, 0], [1, 1], [2, 2], [3, 3]], [[0, 1], [1, 2], [2, 3], [3, 4]], [[0, 2], [1, 3], [2, 4], [3, 5]], // 2nd column moving down + right
    [[1, 0], [2, 1], [3, 2], [4, 3]], [[1, 1], [2, 2], [3, 3], [4, 4]], [[1, 2], [2, 3], [3, 4], [4, 5]], // 3rd column moving down + right
    [[2, 0], [3, 1], [4, 2], [5, 3]], [[2, 1], [3, 2], [4, 3], [5, 4]], [[2, 2], [3, 3], [4, 4], [5, 5]], // 4th column moving down + right
    [[3, 0], [4, 1], [5, 2], [6, 3]], [[3, 1], [4, 2], [5, 3], [6, 4]], [[3, 2], [4, 3], [5, 4], [6, 5]], // 7th column - moving down + left
    [[6, 0], [5, 1], [4, 2], [3, 3]], [[6, 1], [5, 2], [4, 3], [3, 4]], [[6, 2], [5, 3], [4, 4], [3, 5]], // 6th column - moving down + left
    [[5, 1], [4, 2], [3, 3], [2, 4]], [[5, 0], [4, 1], [3, 2], [2, 3]], [[5, 2], [4, 3], [3, 4], [2, 5]], // 5th column - moving down + left
    [[4, 0], [3, 1], [2, 2], [1, 3]], [[4, 1], [3, 2], [2, 3], [1, 4]], [[4, 2], [3, 3], [2, 4], [1, 5]], // 4th column - moving down + left
    [[3, 0], [2, 1], [1, 2], [0, 3]], [[3, 1], [2, 2], [1, 3], [0, 4]], [[3, 2], [2, 3], [1, 4], [0, 5]]];
    /**
     * for loop to loop through all the patterns array length
     * loop breaks if current pattern is incomplete and moves to next
     */

    for (var pidx = 0; pidx < patterns.length; pidx++) {
      // assign a pattern to pattern variable to be checked
      var pattern = patterns[pidx]; // variable to hold current state pattern first co-ordinates to be 

      var winner = state[pattern[0][0]][pattern[0][1]];

      if (winner) {
        //loop over pattern
        for (var idx = 1; idx < pattern.length; idx++) {
          // if pattern not matched 
          if (winner != state[pattern[idx][0]][pattern[idx][1]]) {
            // set winner to undefined and break loop for that pattern
            winner = undefined;
            break;
          }
        } // returns winner value if present


        if (winner) {
          return winner;
        }
      }
    }
    /**
     * for loop to check through columns and rows for any values undefined
     * checks for empty squares
     */
    // check all columns


    for (var x = 0; x <= 6; x++) {
      // check all rows
      for (var y = 0; y <= 5; y++) {
        // if one still has no value the game is still is play
        if (!state[x][y]) {
          return undefined;
        }
      }
    } // if the squares all have a value then the value for a draw is passed.


    return '';
  }
  /** Nested pattern lists for match_pattern_at function
   *  uses type - 'p' for player, x and y co-ordinates directional match 
   *  patterns checking for 'p' at the end allow for matches of any shape or length
   * if 'p' is the current player the co-ordinates are used to match the next value in direction stated
   */


  var patterns = [// Highest scoring patterns - indicate to Heuristic how good the match is. 4 in a row
  {
    // 4 in row up
    pattern: [['p', 0, 1], ['p', 0, 1], ['p', 0, 1], ['p']],
    score: 1000
  }, {
    // 4 in row down
    pattern: [['p', 0, -1], ['p', 0, -1], ['p', 0, -1], ['p']],
    score: 1000
  }, {
    // 4 in row horizontally right
    pattern: [['p', 1, 0], ['p', 1, 0], ['p', 1, 0], ['p']],
    score: 1000
  }, {
    // 4 in a rown horizontally left
    pattern: [['p', -1, 0], ['p', -1, 0], ['p', -1, 0], ['p']],
    score: 1000
  }, {
    // 4 in row diagonally up right
    pattern: [['p', 1, 1], ['p', 1, 1], ['p', 1, 1], ['p']],
    score: 1000
  }, {
    // 4 in a row diagonally down right
    pattern: [['p', 1, -1], ['p', 1, -1], ['p', 1, -1], ['p']],
    score: 1000
  }, {
    // 4 in a row diagonally up left
    pattern: [['p', -1, 1], ['p', -1, 1], ['p', -1, 1], ['p']],
    score: 1000
  }, {
    // 4 in a row diagonally down left
    pattern: [['p', -1, -1], ['p', -1, -1], ['p', -1, -1], ['p']],
    score: 1000
  }, // Second highest scoring matches - 3 in a row
  {
    // 3 in row up
    pattern: [['p', 0, 1], ['p', 0, 1], ['p']],
    score: 500
  }, {
    // 3 in row down
    pattern: [['p', 0, -1], ['p', 0, -1], ['p']],
    score: 500
  }, {
    // 3 in row right
    pattern: [['p', 1, 0], ['p', 1, 0], ['p']],
    score: 500
  }, {
    // 3 in row left
    pattern: [['p', -1, 0], ['p', -1, 0], ['p']],
    score: 500
  }, {
    // 3 in row diagonally up right
    pattern: [['p', 1, 1], ['p', 1, 1], ['p']],
    score: 500
  }, {
    // 3 in a row diagonally down right
    pattern: [['p', 1, -1], ['p', 1, -1], ['p']],
    score: 500
  }, {
    // 3 in a row diagonally up left
    pattern: [['p', -1, 1], ['p', -1, 1], ['p']],
    score: 500
  }, {
    // 3 in a row diagonally down left
    pattern: [['p', -1, -1], ['p', -1, -1], ['p']],
    score: 500
  }, // Third highest scoring matches - 2 in a row
  {
    // 2 up
    pattern: [['p', 0, 1], ['p']],
    score: 250
  }, {
    // 2 down
    pattern: [['p', 0, -1], ['p']],
    score: 250
  }, {
    // 2 right
    pattern: [['p', 1, 0], ['p']],
    score: 250
  }, {
    // 2 left
    pattern: [['p', -1, 0], ['p']],
    score: 250
  }, {
    // 2 in row diagonally up right
    pattern: [['p', 1, 1], ['p']],
    score: 250
  }, {
    // 2 in a row diagonally down right
    pattern: [['p', 1, -1], ['p']],
    score: 250
  }, {
    // 2 in a row diagonally up left
    pattern: [['p', -1, 1], ['p']],
    score: 250
  }, {
    // 2 in a row diagonally down left
    pattern: [['p', -1, -1], ['p']],
    score: 250
  }];
  /**
   * Function to test patterns  in var patterns above against the state
   * @param {*} state 
   * @param {*} pattern 
   * @param {*} player 
   * @param {*} x 
   * @param {*} y 
   * function returns boolean;
   */

  function match_pattern_at(state, pattern, player, x, y) {
    //check x,y are the next to check and within the limits of the state.
    if (x >= 0 && x < state.length) {
      if (y >= 0 && y < state[x].length) {
        // holds the first element out of the pattern in a variable.
        var element = pattern[0]; // type part of element is 'p' for the current player.

        if (element[0] == 'p') {
          // if not current player pattern does not match and marked as false.
          if (state[x][y] !== player) {
            return false;
          }
        } //checks for empty square
        else if (element[0] == ' ') {
            // if not empty pattern does not match and marked as false.
            if (state[x][y] !== undefined) {
              return false;
            }
          } // check that pattern has more than 1 element in it


        if (pattern.length > 1) {
          // recursivley calls itself to check the remainder of the pattern
          return match_pattern_at(state, pattern.slice(1), player, x + element[1], y + element[2]);
        } else {
          // if only 1 element return true as pattern matched
          return true;
        }
      }
    } // returns false when x,y co-ordinates outside the state.


    return false;
  }
  /**
   * Function to check if given pattern matches anywhere in the current state for given player.
   * @param {*} state 
   * @param {*} pattern 
   * @param {*} player 
   */


  function match_pattern(state, pattern, player) {
    // for loops to check every space in the state
    for (var x = 0; x < state.length; x++) {
      for (var y = 0; y < state[x].length; y++) {
        // uses match_pattern_at function to check if pattern matches for player at given co-ordinates.
        var matches = match_pattern_at(state, pattern, player, x, y);

        if (matches) {
          return true;
        }
      }
    }

    return false;
  }
  /**
   * Heurostic approach to take into account the strength of both players positions in the game.
   * Loops through patterns to calculate scores based on positions.
   * Adding to the score benefits the computer, reducing the score shows benefit to the player.
   * A score above 0 shows a state good for the computer, while below 0 is good for the player.
   * Scores are taken from the nexted pattern lists - why better patterns have higher scores.
   * @param {*} state 
   */


  function heuristic(state) {
    var score = 0; // loops through the patterns

    for (var idx = 0; idx < patterns.length; idx++) {
      // if pattern is the computer the score is added
      if (match_pattern(state, patterns[idx].pattern, 'Blue')) {
        score = score + patterns[idx].score;
      } // if pattern is the player the score is taken away


      if (match_pattern(state, patterns[idx].pattern, 'Pink')) {
        score = score - patterns[idx].score;
      }
    } // score is returned to show how good the position is,


    return score;
  }
  /**
   * Recursive function to determine all possible moves for the current state.
   * Repeats for as many levels as the limit parameter is set to.
   * Calculates a heuristic leaf node score.
   * Moves back up the tree to calculate scores for all of a positions nect moves.
   * @param {*} state 
   * @param {*} limit 
   * @param {*} player 
   */


  function minimax(state, limit, player) {
    // list of possible moves.
    var moves = []; // as long as the limit is above zero

    if (limit > 0) {
      // for loops to check through columns (7) and rows (6)
      for (var x = 0; x < 7; x++) {
        for (var y = 0; y < 6; y++) {
          // check if any spaces are undefined meaning they are empty for potential move
          if (state[x][y] === undefined) {
            // idx2 sets the computer counters to start from the bottom row.
            y = 5; // reduces the row by 1 each turn

            while (state[x][y]) {
              y = y - 1;
            } // as long as the current row is greater than or euqal to zero


            if (y >= 0) {
              var move = {
                // x co-ordinate
                x: x,
                // y co-ordinate
                y: y,
                // deepClone called with state and saves next state
                state: deepClone(state),
                score: 0
              }; // state sets to current player.

              move.state[x][y] = player; // checks leaf nodes
              // checks for leaf or internal node status via set limit or if the game has been won.

              if (limit === 1 || check_game_winner(move.state) !== undefined) {
                // calls heuristic function to get score
                move.score = heuristic(move.state);
              } // else checks internal nodes
              // recursively calculates all moves
              else {
                  // mini max called with updated state, limit reduces by one and player switched
                  move.moves = minimax(move.state, limit - 1, player == 'Pink' ? 'Blue' : 'Pink');
                  var score = undefined; // loop over moves - 3 cases

                  for (var idx3 = 0; idx3 < move.moves.length; idx3++) {
                    // score is undefined - assign score
                    if (score === undefined) {
                      score = move.moves[idx3].score;
                    } // current player is human - calculate max score and potential move score
                    else if (player === 'Pink') {
                        score = Math.max(score, move.moves[idx3].score);
                      } // current computer player  - calculate min score and potential move score 
                      else if (player === 'Blue') {
                          score = Math.min(score, move.moves[idx3].score);
                        }
                  } // set the move score to score


                  move.score = score;
                }

              moves.push(move);
            }
          }
        }
      }
    }

    return moves;
  }
  /**
   * Function to add a computer player to play against.
   * Calls minimax funcion with 3 params and sets to move.
   * @param {*} state 
   */


  function computer_move(state) {
    // moves called minimax function
    // Pass in state, number of moves to search ahead and blue player to go first in heuristic
    var moves = minimax(state, 4, 'Blue');
    var max_score = undefined;
    var move = undefined; // loops over the potential moves

    for (var idx = 0; idx < moves.length; idx++) {
      // places a counter if score empty or highest
      if (max_score === undefined || moves[idx].score > max_score) {
        // place sound when computer moves
        createjs.Sound.play('place-counter'); // sets max score to the  move score

        max_score = moves[idx].score; // move co-ordinates

        move = {
          x: moves[idx].x,
          y: moves[idx].y
        };
      }
    }

    return move;
  }
  /**
   * Main ember component
   */


  var _default = Ember._setComponentTemplate(__COLOCATED_TEMPLATE__, Ember.Component.extend({
    // initiate component variables.
    playing: false,
    winner: undefined,
    draw: false,

    /**
     * when component is created init method called
     */
    init: function () {
      //calls parent class to initialise component basic functionality
      this._super(...arguments); //load sounds via method - uses path in public folder and assigns a name for use.


      createjs.Sound.registerSound("assets/sounds/click.mp3", 'place-counter');
      createjs.Sound.registerSound("assets/sounds/counters.mp3", 'falling');
      createjs.Sound.registerSound("assets/sounds/fanfare.mp3", 'fanfare');
      createjs.Sound.registerSound("assets/sounds/loser.mp3", 'loser');
      createjs.Sound.registerSound("assets/sounds/draw.mp3", 'draw');
    },

    /**
     * Function used to insert graphic by hooking into Embers component lifecycle
     * Called once component HTML is inserted into the browser DOM.
     */
    didInsertElement: function () {
      // Uses createjs to create a drawing surface - "Stage"
      var stage = new createjs.Stage(this.element.querySelector("#stage")); // board variable holds the cretejs shape for the board.

      var board = new createjs.Shape(); // graphics variable holding shape grahpics property/

      var graphics = board.graphics; // sets the colour to be drawn
      // using a light grey colour for the board grid

      graphics.beginFill('#D8DBE2'); //drawing horizontal lines - x, y cordinate to start shape, length and width.
      // 7 lines drawn to give 6 rows with the outer lines

      graphics.drawRect(0, 0, 315, 2); // outer line

      graphics.drawRect(0, 49, 315, 2);
      graphics.drawRect(0, 99, 315, 2);
      graphics.drawRect(0, 149, 315, 2);
      graphics.drawRect(0, 199, 315, 2);
      graphics.drawRect(0, 249, 315, 2);
      graphics.drawRect(0, 300, 315, 2); // outer line
      // Vertical lines
      // 8 lines drawn to give 7 columns

      graphics.drawRect(0, 0, 2, 300); // outer line

      graphics.drawRect(45, 0, 2, 300);
      graphics.drawRect(90, 0, 2, 300);
      graphics.drawRect(135, 0, 2, 300);
      graphics.drawRect(180, 0, 2, 300);
      graphics.drawRect(225, 0, 2, 300);
      graphics.drawRect(270, 0, 2, 300);
      graphics.drawRect(315, 0, 2, 300); // outer line
      // set board top left hand co-ordinates to position board on screen

      board.x = 5;
      board.y = 20; // set board opacity to 0 for fade in animation on game start

      board.alpha = 0;
      this.set('board', board); // add the board to the stage to be drawn.

      stage.addChild(board); // variable to hold the player counters
      // pink = human
      // blue = computer

      var counters = {
        'Pink': [],
        'Blue': []
      };
      /**
       * for loop to create 21 counters for each player
       */

      for (var c = 0; c < 21; c++) {
        // new shape for the pink counter
        var pinkCounter = new createjs.Shape();
        graphics = pinkCounter.graphics; // set a dark colour for the outer rim of counter

        graphics.beginFill('#373f51'); // draw the outer square

        graphics.drawRect(-17, -17, 35, 35); //set counter to pink

        graphics.beginFill('#C82586'); // draw the inner square

        graphics.drawRect(-14.5, -14.5, 30, 30); // sets the counter to not initially show on board

        pinkCounter.visible = false; // add the pink counters to the stage

        stage.addChild(pinkCounter); // add all the counters to counters object

        counters.Pink.push(pinkCounter); // new shape for the blue marker

        var blueCounter = new createjs.Shape();
        graphics = blueCounter.graphics; // set a dark colour for the outer rim of counter

        graphics.beginFill('#373f51'); // draw the outer square

        graphics.drawRect(-17, -17, 35, 35); // set counter to blue

        graphics.beginFill('#4EEAF6'); // draw the inner square

        graphics.drawRect(-14.5, -14.5, 30, 30); // set the counter to not initially show

        blueCounter.visible = false; // add the blue counters to the stage

        stage.addChild(blueCounter); // add all the counters to the counters object

        counters.Blue.push(blueCounter);
      } // set counters and stage in component state.


      this.set('counters', counters);
      this.set('stage', stage); // event listener used to refresh board for animations

      createjs.Ticker.addEventListener("tick", stage);
    },

    /**
     * Click function allows the game to be played if playing and no winner 
     * sets click area to the board
     * sets column and row values
     * sets move count and checks for a winner.
     * originally allowed two players - code refactored for human and computer functionality.
     * @param {*} ev 
     */
    click: function (ev) {
      // set the component to this
      var component = this; // checks for clicks if playing and no winner already 

      if (component.get('playing') && !component.get('winner')) {
        // sets the board area for clicks
        // browser click event checks if canvas or set area between x,y has been clicked
        if (ev.target.tagName.toLowerCase() == 'canvas' && ev.offsetX >= 5 && ev.offsetY >= 20 && ev.offsetX < 315 && ev.offsetY < 315) {
          //  allows for board x position and sets column widths
          var x = Math.floor((ev.offsetX - 5) / 45); // allows for board y position and sets row widths

          var y = Math.floor((ev.offsetY - 20) / 45); // stores the component state in a local state variable

          var state = component.get('state'); // set y co-ordinate to bottom row for bottom to play from bottom up

          var y = 5; // while co-ordinates are available - minus 1 from y to play a row up

          while (state[x][y]) {
            y = y - 1;
          }
          /**
           * check co-ordinates and stop y overspilling the board at 0
           */


          if (!state[x][y] && y >= 0) {
            // set co-ordinates to the counter
            state[x][y] = 'Pink'; // play sound for placing counter

            createjs.Sound.play('place-counter'); // save the component moves for pink to move count variable

            var move_count = component.get('moves')['Pink']; // gets the next counter 

            var counter = component.get('counters')['Pink'][move_count]; // make the counter visible when played

            counter.visible = true; // counter co-ordinates

            counter.x = 28 + x * 45;
            counter.y = 45 + y * 50; // sets player ready and updates move count

            component.get('moves')['Pink'] = move_count + 1; // call componect check winner function and update

            component.check_winner(); // computer move functionality wrapped in timeout to 'simulate' thinking
            // slight delay added but only very small as to not be annoying.
            // calls computer move function with current state to find a suitable position if still playing

            setTimeout(function () {
              if (!component.get('winner') && !component.get('draw')) {
                var move = computer_move(state);
                move_count = component.get('moves')['Blue'];
                state[move.x][move.y] = 'Blue';
                counter = component.get('counters')['Blue'][move_count];
                counter.x = 28 + move.x * 45;
                counter.y = 45 + move.y * 50;
                counter.visible = true;
                component.get('moves')['Blue'] = move_count + 1; // component.get('stage').update();

                component.check_winner();
              } // delay to computer player to stop human player cheating by placing counters quickly in a row

            }, 200);
          }
        }
      }
    },

    /**
     * refactored to use the components check winner function
     */
    check_winner: function () {
      // stores component state in variable
      var state = this.get('state'); // stores component state

      var winner = check_game_winner(state); // whatever is returned is set to the component state and appropriate sound played

      if (winner !== undefined) {
        if (winner === '') {
          // crowd 'ooo' if theres a draw
          this.set('draw', true);
          createjs.Sound.play("draw");
        } else {
          this.set('winner', winner);

          if (winner == "Pink") {
            // fanfare for player win
            createjs.Sound.play("fanfare");
          } else {
            // retro lose sound for computer win
            createjs.Sound.play("loser");
          }
        }
      }
    },

    /**
     * actions  is component child object to define actions required
     * board is defined, cleared + start/restart
     * checks if game is playing
     * animation used to clear board + sound
     */
    actions: {
      start: function () {
        // initiates board state
        var board = this.get('board'); // sets board opacity to zero so not shown on start

        board.alpha = 0; // if game is playing

        if (this.get('playing')) {
          // get counters from component
          var counters = this.get('counters'); // animate counters on restart

          for (var c = 0; c < 21; c++) {
            createjs.Tween.get(counters.Pink[c]).to({
              y: 650
            }, 900);
            createjs.Tween.get(counters.Blue[c]).to({
              y: 650
            }, 900);
          } // sound effect for counters falling off board


          createjs.Sound.play("falling"); // fades in board after retart

          createjs.Tween.get(board).wait(500).to({
            alpha: 1
          }, 1000);
        } else {
          // fades in board
          createjs.Tween.get(board).to({
            alpha: 1
          }, 1000);
        } // game set to play


        this.set('playing', true); // winner and draw iniatiated

        this.set('winner', undefined);
        this.set('draw', undefined); // game state stored as nested arrays used to define board structure.
        // positions start undefined until selected

        this.set('state', [[undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined]]); // set each player moves to zero for start of game

        this.set('moves', {
          'Pink': 0,
          'Blue': 0
        }); // sets first player to pink

        this.set('player', 'Pink');
      }
    }
  }));

  _exports.default = _default;
});
;define("web-app/components/welcome-page", ["exports", "ember-welcome-page/components/welcome-page"], function (_exports, _welcomePage) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _welcomePage.default;
    }
  });
});
;define("web-app/data-adapter", ["exports", "@ember-data/debug"], function (_exports, _debug) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _debug.default;
    }
  });
});
;define("web-app/helpers/app-version", ["exports", "web-app/config/environment", "ember-cli-app-version/utils/regexp"], function (_exports, _environment, _regexp) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.appVersion = appVersion;
  _exports.default = void 0;

  function appVersion(_, hash = {}) {
    const version = _environment.default.APP.version; // e.g. 1.0.0-alpha.1+4jds75hf
    // Allow use of 'hideSha' and 'hideVersion' For backwards compatibility

    let versionOnly = hash.versionOnly || hash.hideSha;
    let shaOnly = hash.shaOnly || hash.hideVersion;
    let match = null;

    if (versionOnly) {
      if (hash.showExtended) {
        match = version.match(_regexp.versionExtendedRegExp); // 1.0.0-alpha.1
      } // Fallback to just version


      if (!match) {
        match = version.match(_regexp.versionRegExp); // 1.0.0
      }
    }

    if (shaOnly) {
      match = version.match(_regexp.shaRegExp); // 4jds75hf
    }

    return match ? match[0] : version;
  }

  var _default = Ember.Helper.helper(appVersion);

  _exports.default = _default;
});
;define("web-app/helpers/pluralize", ["exports", "ember-inflector/lib/helpers/pluralize"], function (_exports, _pluralize) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var _default = _pluralize.default;
  _exports.default = _default;
});
;define("web-app/helpers/singularize", ["exports", "ember-inflector/lib/helpers/singularize"], function (_exports, _singularize) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var _default = _singularize.default;
  _exports.default = _default;
});
;define("web-app/initializers/app-version", ["exports", "ember-cli-app-version/initializer-factory", "web-app/config/environment"], function (_exports, _initializerFactory, _environment) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  let name, version;

  if (_environment.default.APP) {
    name = _environment.default.APP.name;
    version = _environment.default.APP.version;
  }

  var _default = {
    name: 'App Version',
    initialize: (0, _initializerFactory.default)(name, version)
  };
  _exports.default = _default;
});
;define("web-app/initializers/container-debug-adapter", ["exports", "ember-resolver/resolvers/classic/container-debug-adapter"], function (_exports, _containerDebugAdapter) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var _default = {
    name: 'container-debug-adapter',

    initialize() {
      let app = arguments[1] || arguments[0];
      app.register('container-debug-adapter:main', _containerDebugAdapter.default);
      app.inject('container-debug-adapter:main', 'namespace', 'application:main');
    }

  };
  _exports.default = _default;
});
;define("web-app/initializers/ember-data-data-adapter", ["exports", "@ember-data/debug/setup"], function (_exports, _setup) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _setup.default;
    }
  });
});
;define("web-app/initializers/ember-data", ["exports", "ember-data", "ember-data/setup-container"], function (_exports, _emberData, _setupContainer) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  /*
    This code initializes EmberData in an Ember application.
  
    It ensures that the `store` service is automatically injected
    as the `store` property on all routes and controllers.
  */
  var _default = {
    name: 'ember-data',
    initialize: _setupContainer.default
  };
  _exports.default = _default;
});
;define("web-app/initializers/export-application-global", ["exports", "web-app/config/environment"], function (_exports, _environment) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.initialize = initialize;
  _exports.default = void 0;

  function initialize() {
    var application = arguments[1] || arguments[0];

    if (_environment.default.exportApplicationGlobal !== false) {
      var theGlobal;

      if (typeof window !== 'undefined') {
        theGlobal = window;
      } else if (typeof global !== 'undefined') {
        theGlobal = global;
      } else if (typeof self !== 'undefined') {
        theGlobal = self;
      } else {
        // no reasonable global, just bail
        return;
      }

      var value = _environment.default.exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = Ember.String.classify(_environment.default.modulePrefix);
      }

      if (!theGlobal[globalName]) {
        theGlobal[globalName] = application;
        application.reopen({
          willDestroy: function () {
            this._super.apply(this, arguments);

            delete theGlobal[globalName];
          }
        });
      }
    }
  }

  var _default = {
    name: 'export-application-global',
    initialize: initialize
  };
  _exports.default = _default;
});
;define("web-app/instance-initializers/ember-data", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  /* exists only for things that historically used "after" or "before" */
  var _default = {
    name: 'ember-data',

    initialize() {}

  };
  _exports.default = _default;
});
;define("web-app/router", ["exports", "web-app/config/environment"], function (_exports, _environment) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  class Router extends Ember.Router {
    constructor(...args) {
      super(...args);

      _defineProperty(this, "location", _environment.default.locationType);

      _defineProperty(this, "rootURL", _environment.default.rootURL);
    }

  } // Path configuration to URL changed 


  _exports.default = Router;
  Router.map(function () {
    this.route('game', {
      path: '/'
    });
  });
});
;define("web-app/routes/game", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  class GameRoute extends Ember.Route {}

  _exports.default = GameRoute;
});
;define("web-app/serializers/-default", ["exports", "@ember-data/serializer/json"], function (_exports, _json) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _json.default;
    }
  });
});
;define("web-app/serializers/-json-api", ["exports", "@ember-data/serializer/json-api"], function (_exports, _jsonApi) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _jsonApi.default;
    }
  });
});
;define("web-app/serializers/-rest", ["exports", "@ember-data/serializer/rest"], function (_exports, _rest) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _rest.default;
    }
  });
});
;define("web-app/services/store", ["exports", "ember-data/store"], function (_exports, _store) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _store.default;
    }
  });
});
;define("web-app/templates/application", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.HTMLBars.template({
    "id": "4+BHWLi3",
    "block": "{\"symbols\":[],\"statements\":[[10,\"section\"],[14,1,\"app\"],[12],[2,\"\\n\"],[2,\"    \"],[10,\"header\"],[12],[2,\"\\n        \"],[10,\"img\"],[14,1,\"logo\"],[14,\"src\",\"assets/images/c4_logo.png\"],[14,\"alt\",\"Connect 4 logo\"],[12],[13],[2,\"\\n    \"],[13],[2,\"\\n\"],[2,\"    \"],[10,\"article\"],[12],[2,\"\\n        \"],[1,[30,[36,1],[[30,[36,0],null,null]],null]],[2,\"\\n    \"],[13],[2,\"\\n\"],[2,\"    \"],[10,\"footer\"],[12],[2,\"\\n        \"],[10,\"div\"],[14,0,\"float-left\"],[12],[2,\"\\n            Powered by Ember\\n        \"],[13],[2,\"\\n        \"],[10,\"div\"],[14,0,\"float-right\"],[12],[2,\"\\n            © Wade Phillips 23944129\\n        \"],[13],[2,\"\\n    \"],[13],[2,\"\\n\"],[13]],\"hasEval\":false,\"upvars\":[\"-outlet\",\"component\"]}",
    "meta": {
      "moduleName": "web-app/templates/application.hbs"
    }
  });

  _exports.default = _default;
});
;define("web-app/templates/game", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.HTMLBars.template({
    "id": "nl4IQOt7",
    "block": "{\"symbols\":[],\"statements\":[[2,\"\\n\"],[1,[34,0]]],\"hasEval\":false,\"upvars\":[\"c-4\"]}",
    "meta": {
      "moduleName": "web-app/templates/game.hbs"
    }
  });

  _exports.default = _default;
});
;define("web-app/transforms/boolean", ["exports", "@ember-data/serializer/-private"], function (_exports, _private) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _private.BooleanTransform;
    }
  });
});
;define("web-app/transforms/date", ["exports", "@ember-data/serializer/-private"], function (_exports, _private) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _private.DateTransform;
    }
  });
});
;define("web-app/transforms/number", ["exports", "@ember-data/serializer/-private"], function (_exports, _private) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _private.NumberTransform;
    }
  });
});
;define("web-app/transforms/string", ["exports", "@ember-data/serializer/-private"], function (_exports, _private) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function () {
      return _private.StringTransform;
    }
  });
});
;

;define('web-app/config/environment', [], function() {
  var prefix = 'web-app';
try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(decodeURIComponent(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

});

;
          if (!runningTests) {
            require("web-app/app")["default"].create({"name":"web-app","version":"0.0.0+a9a33cbe"});
          }
        
//# sourceMappingURL=web-app.map
